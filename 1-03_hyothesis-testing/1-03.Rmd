---
title: An R Markdown document converted from "1-03.ipynb"
output: html_document
---

```{r}
library(tidyverse)

theme_set(theme_minimal())
```

# Test statistic for paired two-sample data

```{r}
# DATA
x1 = c(14.976, 7.040, 11.588, 14.460, 6.876, 9.258, 10.444, 5.425, 12.508, 11.844, 7.695, 4.642, 6.916, 2.947, 10.423, 9.558, 10.783)
x2 = c(16.294, 8.406, 11.623, 15.667, 7.141, 10.679, 10.698, 6.28, 11.474, 13.021, 6.753, 4.477, 7.284, 4.308, 10.174, 9.618, 11.447)

xD = x2-x1
n = length(xD)

xD %>% round(3)
```

```{r}
# Calculate the sample statistic
sample.statistic = mean(xD)/(sd(xD)/sqrt(n))
sample.statistic
```

```{r}
alpha = .05
qalpha = qt(1-alpha/2, df=n-1)

# Locate the sample statistic on the t-distribution 

options(repr.plot.width = 4, repr.plot.height = 3)

ggplot(data = data.frame(x = c(-4, 4)), aes(x)) +

# Extreme areas
stat_function(fun = dt, args = list(df = n-1), geom = "area", xlim = c(sample.statistic,4), fill = "turquoise") +
stat_function(fun = dt, args = list(df = n-1), geom = "area", xlim = c(-4,-sample.statistic), fill = "darkorange") +

# # Critical regions
# stat_function(fun = dt, args = list(df = n-1), geom = "area", xlim = c(qalpha,4), fill = "red") +
# stat_function(fun = dt, args = list(df = n-1), geom = "area", xlim = c(-4,-qalpha), fill = "red") +

stat_function(fun = dt, args = list(df = n-1), color = "black") +
geom_segment(x = sample.statistic, xend = sample.statistic, y = 0, yend = dt(sample.statistic, df=n-1), linetype = 1, color = "black") +

annotate(geom = "text", label = paste0("t=",round(sample.statistic,2)), x = sample.statistic, y = 0.0, hjust= 1.1) +
labs(x = paste0("t-distribution with ",n," dof"), y = "density")

# Area equal or higher than the sample statistic ("one-tail" t-test)
1-pt(sample.statistic, df=n-1)

# Area equal or *more extreme* (both high and low) than the sample statistic ("two-tail" t-test)
2*(1-pt(sample.statistic, df=n-1))
```

```{r}
sample.statistic
t.test(xD)
```

```{r}
t.test(x2,x1, paired=T)
```

```{r}
chisq.test
```

# Relationship between hypothesis testing and confidence interval

```{r}
x_mean = mean(xD)
s = sd(xD)


conflvl = .95
confint_low = x_mean - qt(1-(1-conflvl)/2, df=n-1)*s/sqrt(n)
confint_high = x_mean + qt(1-(1-conflvl)/2, df=n-1)*s/sqrt(n)

c(confint_low, confint_high) %>% round(3)
```

